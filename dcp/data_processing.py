import os
import sys
import pickle
import hashlib
import socket
import time

from uuid import uuid4
from abc import (
    ABCMeta,
    abstractmethod,
    abstractstaticmethod,
)

from tqdm import tqdm

from exceptions import PacketIsDamagedError


class NetData:
    def __init__(self, net_data):
        self.data = net_data[0]
        self.address = net_data[1]


class Packet:
    __slots__ = ('ack', 'data')

    def __init__(self, ack, data):
        self.ack = ack
        self.data = data

    def __bytes__(self):
        return pickle.dumps(self)

    def __str__(self):
        return 'Packet(ack=%s, data="%s")' % (self.ack, self.data)

    def __repr__(self):
        return 'Packet(ack=%s, data="%s")' % (self.ack, self.data)

    def confirm(self, ack=None):
        self.ack += 1
        self.data = None
        if ack:
            self.ack = ack


class IBaseNetFile:

    @staticmethod
    @abstractmethod
    def is_start_packet(packet: Packet) -> bool:
        """"""

    @staticmethod
    @abstractmethod
    def is_data_packet(packet: Packet) -> bool:
        """"""

    @staticmethod
    @abstractmethod
    def is_end_packet(packet: Packet) -> bool:
        """"""

    @staticmethod
    @abstractmethod
    def is_error_packet(packet: Packet) -> bool:
        """"""

    @abstractmethod
    def receive(self, packet: Packet, *args, **kwargs):
        """"""

    @abstractmethod
    def get_hash(self):
        """"""


class BaseNetFile(IBaseNetFile):
    DATA = 1
    START = 0
    END = -1
    ERROR = -2
    HASH_SUM_ERROR_MESSAGE = 'Hash error'
    PACKET_MIN_SIZE = sys.getsizeof(Packet(None, None)) + 1

    def __init__(self, path=None, buffer=None, sha=None):
        self.path = path or 'dcp_%s.file' % (uuid4(),)
        self.buffer = buffer or []
        self.sha = hashlib.sha256()

    @staticmethod
    def is_start_packet(packet):
        return packet.ack == ReceiveNetFile.START

    @staticmethod
    def is_data_packet(packet):
        return packet.ack >= ReceiveNetFile.DATA

    @staticmethod
    def is_end_packet(packet):
        return packet.ack == ReceiveNetFile.END

    @staticmethod
    def is_error_packet(packet):
        return packet.ack == ReceiveNetFile.ERROR

    def receive(self, packet, refresh_obj=True, save=True):
        if ReceiveNetFile.is_start_packet(packet):
            self.path = packet.data
        elif ReceiveNetFile.is_data_packet(packet):
            if packet.data:
                self.buffer.append(packet.data)
                self.sha.update(packet.data)
        elif ReceiveNetFile.is_end_packet(packet):

            if save:
                self.save()

            if self.check_hash(packet) is False:
                packet.confirm(self.ERROR)
                return packet

        packet.confirm()
        return packet

    def get_hash(self):
        return self.sha.hexdigest()


class ReceiveNetFile(BaseNetFile):
    def check_hash(self, packet):
        return packet.data == self.get_hash()

    def save(self):
        with open(self.path, 'wb') as f:
            f.write(
                b''.join(self.buffer),
            )

    def refresh(self):
        self.__init__()


class SendNetFile(BaseNetFile):
    def __init__(
            self,
            path,
            destination_address,
            sock,
            buffer_size,
            retry_send_count,
            socket_timeout,
            payload_size,
    ):
        self.path = path
        self.destination_address = destination_address
        self.sock = sock
        self.buffer_size = buffer_size
        self.retry_send_count = retry_send_count
        self.socket_timeout = socket_timeout
        self.buffer = []
        self.sha = hashlib.sha256()
        self.packet_num = self.START
        self.payload_size = payload_size

    def read(self, chunk) -> bytes:
        while True:
            data = sys.stdin.buffer.read(chunk)
            if not data:
                break
            self.buffer.append(data)
            self.sha.update(data)

        return self.buffer

    def get_start_packet(self):
        return Packet(self.START, self.path)

    def is_hand_shaked(self, packet):
        return packet.ack > self.START

    def hand_shake(self):
        self.sock.sendto(
            bytes(self.get_start_packet()),
            self.destination_address,
        )
        try:
            net_data = NetData(self.sock.recvfrom(self.buffer_size))
        except socket.timeout:
            self.sock.close()
            print('Socket timeout')
            sys.exit(1)
        try:
            packet = pickle.loads(net_data.data)
        except (pickle.UnpicklingError, EOFError):
            print('Unpickling error')
            self.sock.close()
            sys.exit(1)

        if not self.is_hand_shaked(packet):
            print('Not hand shaked')
            self.sock.close()
            sys.exit(1)

        self.packet_num = packet.ack

    def send(self):
        self.read(self.buffer_size - self.PACKET_MIN_SIZE)
        progress_bar = tqdm(
            total=self.payload_size,
            ascii=True,
            desc='Progress',
            dynamic_ncols=True)

        for data in self.buffer:
            packet_to_send = Packet(self.packet_num, data)
            self.sock.sendto(bytes(packet_to_send),
                             self.destination_address)

            net_data = None
            for _ in range(self.retry_send_count):

                try:
                    net_data = NetData(self.sock.recvfrom(self.buffer_size))
                except socket.timeout:
                    self.sock.sendto(bytes(packet_to_send),
                                     self.destination_address)
                    pass

                try:
                    packet_received = pickle.loads(net_data.data)
                except (pickle.UnpicklingError, AttributeError):
                    print('unpickling error')
                    continue
                except EOFError:
                    break

                if packet_received.ack > self.packet_num:
                    self.packet_num = packet_received.ack
                    progress_bar.update(self.buffer_size - self.PACKET_MIN_SIZE)
                    break
                else:
                    self.sock.sendto(bytes(packet_to_send),
                                     self.destination_address)

        packet = Packet(-1, self.get_hash())
        self.sock.sendto(bytes(packet), self.destination_address)
        progress_bar.close()

    def receive(self, packet, refresh_obj=True, save=True):
        packet.confirm()
        return packet
