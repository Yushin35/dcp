from contextlib import contextmanager

import pytest

from .data_processing import ArqProcess
from .exceptions import PacketIsDamagedError


@contextmanager
def not_raises(exception):
    try:
        yield
    except exception:
        raise pytest.fail("DID RAISE {0}".format(exception))


class TestArqProcess:
    def test_get_hash_sum(self):

        assert (
            b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40b' ==
            ArqProcess.get_hash_sum(b'import pytest')
        )

    def test_add_hash_sum(self):
        assert (
            b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40bimport pytest' ==
            ArqProcess.add_hash_sum(b'import pytest')
        )

    def test_fetch_hash_sum(self):
        assert (
            b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40b' ==
            ArqProcess.fetch_hash_sum(b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40bimport pytest')
        )

    def test_fetch_data(self):
        assert (
            b'import pytest' ==
            ArqProcess.fetch_data(b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40bimport pytest')
        )

    def test_check_integrity(self):
        with not_raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40bimport pytest')

    def test_fetch_hash_sum_fail(self):
        with pytest.raises(PacketIsDamagedError):
            assert (
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40b' !=
                ArqProcess.fetch_hash_sum(b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f4')
            )

    def test_fetch_data_fail(self):
        with pytest.raises(PacketIsDamagedError):
            assert (
                b'import pytest' ==
                ArqProcess.fetch_data(
                    b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f4')
            )

    def test_check_integrity_fail_content(self):
        with pytest.raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40bimport pytes')

    def test_check_integrity_fail_hash_sum(self):
        with pytest.raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40import pytest')

    def test_check_integrity_fail_hash_both(self):
        with pytest.raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40import pytes')

    def test_check_integrity_fail_length(self):
        with pytest.raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'db7b0741c94798db5932296122312b7b87d6e5a9e2ee29833a9949d1f774f40')

    def test_check_integrity_fail_empty(self):
        with pytest.raises(PacketIsDamagedError):
            ArqProcess.check_integrity(
                b'')
