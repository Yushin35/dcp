import os
import argparse


from exceptions import (
    DestinationParseError,
    ValidationError,
)
from validators import (
    validate_ip,
    validate_port,
    check_positive,
)


def parse_destination(destination: str) -> tuple:
    destination_list = destination.split(':')
    if len(destination_list) != 2:
        raise DestinationParseError(
            'Cannot parse destination string [%s]. '
            'Format <IP>:<PORT>' % (destination, ))
    ip, port = destination_list
    return validate_ip(ip), validate_port(port)


class ParserArgs:
    def __init__(self):

        self.BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        self.default_logging = '/tmp/arq.log'
        self.default_handshake_timer = 5
        self.default_socket_timeout = 1
        self.default_buffer_size = 40960
        self.default_retry_send_count = 500

        self.parser = argparse.ArgumentParser(
            description='arq command line option')

        self.parser.add_argument(
            '-s', '--server',
            action='store_true',
            required=False,
            help='Run arq as server',
        )
        self.parser.add_argument(
            '-d', '--destination',
            type=str,
            required=True,
            help='Destination <IP>:<PORT>',
        )
        self.parser.add_argument(
            '-t', '--socket_timeout',
            type=check_positive,
            default=self.default_socket_timeout,
            required=False,
            help='Socket timeout. Default %s sec' % (
                self.default_socket_timeout,
            ),
        )
        self.parser.add_argument(
            '-r', '--retry_send_count',
            type=check_positive,
            default=self.default_retry_send_count,
            required=False,
            help='Retry send count. Default %s' % (
                self.default_retry_send_count,
            ),
        )
        self.parser.add_argument(
            '-f', '--output_file',
            type=str,
            required=False,
            help='Output file',
        )
        self.parser.add_argument(
            '-b', '--buffer_size',
            type=int,
            default=self.default_buffer_size,
            required=False,
            help='Buffer receive|send size. Default - %s(bytes)' % (
                self.default_buffer_size,
            ),
        )
        self.parser.add_argument(
            '-l',
            '--logging',
            type=str,
            default=self.default_logging,
            required=False,
            help='Logging file',
        )

        self.args = self.parser.parse_args()

    def get_as_server(self):
        return self.args.server

    def get_destination_string(self):
        return self.args.destination

    def get_handshake_timer(self):
        return self.args.handshake_timer

    def get_logging(self):
        return self.args.logging

    def get_output_file(self):
        return self.args.output_file

    def get_buffer_size(self):
            return self.args.buffer_size

    def get_socket_timeout(self):
        return self.args.socket_timeout

    def get_retry_send_count(self):
        return self.args.retry_send_count
