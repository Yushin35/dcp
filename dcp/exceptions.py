class BaseArqException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message


class DestinationParseError(BaseArqException):
    pass


class HandshakeTimeoutError(BaseArqException):
    pass


class ValidationError(BaseArqException):
    pass


class PacketIsDamagedError(BaseArqException):
    pass


class PacketUnpicklingError(Exception):
    message = 'unpickling error'
