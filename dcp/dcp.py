#!/home/user/work/projects/grafana/.env/bin/python3

import os
import sys
import pickle
import hashlib

import socket
import logging

from uuid import uuid4


from tqdm import tqdm

from exceptions import (
    HandshakeTimeoutError,
    DestinationParseError,
    ValidationError,
    PacketIsDamagedError,
    PacketUnpicklingError,
)
from parsers import (
    ParserArgs,
    parse_destination,
)
from ui import (
    start_info,
    progress_info,
    end_info,
)
from data_processing import (
    Packet,
    ReceiveNetFile,
    SendNetFile,
    NetData,
)
from utils import get_human_readable_size


def client(
        ip,
        port,
        buffer_size,
        server_file_path,
        server_mode,
        logging_file,
        socket_timeout,
        retry_send_count,
):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.settimeout(socket_timeout)

        destination = (ip, port)
        payload_size = os.fstat(sys.stdin.fileno()).st_size

        start_info(
            server_mode,
            socket_timeout,
            destination,
            get_human_readable_size(payload_size),
            logging_file,
            retry_send_count,
        )

        net_file = SendNetFile(
            server_file_path,
            destination,
            sock,
            buffer_size,
            retry_send_count,
            socket_timeout,
            payload_size,
        )
        net_file.hand_shake()
        net_file.send()
        end_info()


def server(
        ip,
        port,
        buffer_size,
        logging_file,
        socket_timeout,
):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        start_info(
            True,
            socket_timeout,
            (ip, port),
            'Unknown',
            logging_file,
            'unavailable in server mode'
        )
        net_file = ReceiveNetFile()

        sock.bind((ip, port))
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        logging.debug('Server listen %s:%s', ip, port)

        while True:
            net_data = NetData(sock.recvfrom(buffer_size))
            try:
                packet = pickle.loads(net_data.data)
            except pickle.UnpicklingError:
                sock.sendto(
                    bytes(Packet(ReceiveNetFile.ERROR,
                          PacketUnpicklingError.message)),
                    net_data.address,
                )
                continue
            except EOFError:
                continue
            answer_packet = net_file.receive(packet)
            sock.sendto(
                bytes(
                    answer_packet,
                ),
                net_data.address,
            )


def main():
    parser_args = ParserArgs()
    server_mode = parser_args.get_as_server()
    destination = parser_args.get_destination_string()
    socket_timeout = parser_args.get_socket_timeout()
    retry_send_count = parser_args.get_retry_send_count()
    buffer_size = parser_args.get_buffer_size()
    logging_file = parser_args.get_logging()
    output_file = parser_args.get_output_file()

    logging.basicConfig(
        format='%(asctime)s - \
                %(name)s - \
                %(levelname)s - \
                %(message)s',
        filename=logging_file,
        level=logging.DEBUG,
    )

    try:
        ip, port = parse_destination(destination)
    except (DestinationParseError, ValidationError) as err:
        logging.error(err.message)
        print(err.message, file=sys.stderr)
        return 1

    if server_mode:
        server(
            ip,
            port,
            buffer_size,
            logging_file,
            socket_timeout,
        )
    else:
        client(
            ip,
            port,
            buffer_size,
            output_file,
            server_mode,
            logging_file,
            socket_timeout,
            retry_send_count,
        )


if __name__ == '__main__':
    exit(main())
