import ipaddress
import argparse

from exceptions import ValidationError


def validate_ip(ip):
    try:
        return str(ipaddress.ip_address(ip))
    except ValueError:
        raise ValidationError('Cannot convert to ip address [%s]' % (ip, ))


def validate_port(port):
    port_min = 1
    port_max = 65535
    if port.isdigit() and port_min <= int(port) <= port_max:
        return int(port)
    raise ValidationError('Cannot convert to ip port [%s]. '
                          'Must be digit in range [%s, %s]' % (port, port_min, port_max))


def check_positive(value):
    ivalue = int(value)
    if ivalue <= 0:
         raise argparse.ArgumentTypeError("%s is an invalid positive int value" % value)
    return ivalue
