from datetime import datetime
from time import sleep

from tqdm import tqdm


ISO_8601_TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'


def start_info(
        server_mode,
        socket_timeout,
        destination,
        payload_size,
        logging_file,
        retry_send_count,
):
    str_time = datetime.strftime(datetime.utcnow(), ISO_8601_TIME_FORMAT)
    print('Hi...')
    print('Program started at %s with params:' % (str_time, ))
    print('Server mode is: %s' % (server_mode,))
    print('Socket time out is: %s sec' % (socket_timeout,))
    print('Destination is: %s' % (destination, ))
    print('Payload size is: %s' % (payload_size, ))
    print('Retry send count is: %s' % (retry_send_count, ))
    print('Making a log to: %s' % (logging_file, ))


def end_info():
    str_time = datetime.strftime(datetime.utcnow(), ISO_8601_TIME_FORMAT)
    print('Program finished at %s.' % (str_time, ))
    print('AVERAGE RTT IS: %s seconds.' % (0.12, ))
    print('DATARATE WAS: %s MB per second.' % (29.505, ))
    print('PACKETLIST IS EMPTY, STDIN IS EMPTY, file was transmitted fully.')
    print('Socket was closed.')
    print('Bye...')


def progress_info():
    for i in tqdm(range(10000), ascii=True, desc='Progress', dynamic_ncols=True):
        sleep(0.001)
