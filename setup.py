import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

requires = [
    'tqdm',
]

setuptools.setup(
    name="arq",
    version="0.0.1",
    author="John Connor",
    author_email="",
    description="App for transfer large file",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/Yushin35/arg",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS linux",
    ],
    install_requires=requires,
    python_requires='>=3.6',
)
